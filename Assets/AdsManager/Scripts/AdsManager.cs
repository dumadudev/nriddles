﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdsManager : MonoBehaviour
{
    public static AdsManager instance = null;
    private InterstitialAd interstitialAd;
    private RewardedAd rewardedAd;
    private Action rewardVideoCallback;
    bool isIAdLoading, isRewAdLoading;

    private void Awake()
    {
        instance = this;
    }

    public void Start()
    {

        // Initialize hbjkthe Google Mobile Ads SDK.
         MobileAds.Initialize((initStatus) =>
        {           
            RequestAndLoadInterstitialAd();
        });
        RequestAndLoadRewardedAd();
    }
    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
            .AddKeyword("unity-admob-sample")
            .Build();
    }
  #region Interstitial
       public void RequestAndLoadInterstitialAd()
    {
        if (interstitialAd != null && interstitialAd.IsLoaded() && isIAdLoading)
        {
            Debug.Log("Al ready loaded interstitialAd ");
            return;
        }
        Debug.Log("Requesting Interstitial Ad.");
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-4489546446191995/9840554470";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Clean up interstitial before using it
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }
        isIAdLoading = true;
        interstitialAd = new InterstitialAd(adUnitId);
        interstitialAd.OnAdLoaded += (sender, args) =>
        {
            Debug.Log("interstitialAd loaded");
            isIAdLoading = false;
        };
        interstitialAd.OnAdFailedToLoad += (sender, args) =>
        {
            RequestAndLoadInterstitialAd();
            Debug.Log("interstitialAd failed to load");
            isIAdLoading = false;
        };
        // Called when an ad is shown.
        this.interstitialAd.OnAdOpening += HandleOnAdOpened;
        // Called when the ad is closed.
        this.interstitialAd.OnAdClosed += HandleOnAdClosed;

        interstitialAd.LoadAd(CreateAdRequest());
    }
  public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        RequestAndLoadInterstitialAd();
        MonoBehaviour.print("HandleAdClosed event received");
    }
    public void ShowInterstitialAd()
    {
        Debug.Log("ShowInterstitialAd");
        if (interstitialAd.IsLoaded())
        {
            interstitialAd.Show();
        }
        else
        {
            Debug.Log("Interstitial ad is not ready yet");
        }
    }
    public void DestroyInterstitialAd()
    {
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
            RequestAndLoadInterstitialAd();
        }
    }
    #endregion

    #region Rewarded
 private void RequestAndLoadRewardedAd()
    {
        if (rewardedAd != null && rewardedAd.IsLoaded() && isRewAdLoading)
        {
            Debug.Log("Al ready loaded rewardedAd ");
            return;
        }
        Debug.Log("Requesting Rewarded Ad.");
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-4489546446191995/5350645121";
#else
        string adUnitId = "unexpected_platform";
#endif

        isRewAdLoading = true;
        rewardedAd = new RewardedAd(adUnitId);
        rewardedAd.OnAdLoaded += (sender, args) =>
        {
            isRewAdLoading = false;
            Debug.Log("Reward Ad loaded");
        };
        rewardedAd.OnAdFailedToLoad += (sender, args) =>
        {
            isRewAdLoading = false;
            RequestAndLoadRewardedAd();
            Debug.Log("Reward Ad failed to load");
        };
        
        rewardedAd.OnUserEarnedReward += (sender, args) =>
        {
            Debug.Log("rewarded!!!!!!!!!");
            //rewardVideoCallback = callBack;
            rewardVideoCallback?.Invoke();
        };
        rewardedAd.OnAdClosed += (sender, args) =>
        {
            RequestAndLoadRewardedAd();
        };
        rewardedAd.LoadAd(CreateAdRequest());
    }

    public void ShowRewardedAd(Action callBack)
    {
        rewardVideoCallback = callBack;
        Debug.Log("ShowRewardedAd");
        if (rewardedAd != null)
        {
            rewardedAd.Show();
        }
        else
        {
            RequestAndLoadRewardedAd();
            Debug.Log("Rewarded ad is not ready yet.");
        }
    }

    #endregion

}
